MY_PATH="/global/dna/projectdirs/microbial/img_web_data_secondary/taxon.faa"
SCRIPT_PATH="/global/u2/v/vsevim/s/fasta_util"

MY_PATH="/global/dna/projectdirs/microbial/img_web_data_secondary/taxon.faa"
SCRIPT_PATH="/global/u2/v/vsevim/s/fasta_util"

genomes_file="$1"
outfile="$2"
echo "converting to tsv..."

while read line; do 
	fname=`echo $line | awk '{printf "%s%s", $1, ".faa"}'` 
	if [ -e "$MY_PATH/$fname" ]; then
		python  "$SCRIPT_PATH/linearize_fasta_with_gene_id.py" "$MY_PATH/$fname"
	fi		
done<$genomes_file > "$outfile"

echo "convertied to tsv."
import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-m",
    "--min_pid",
    nargs="?",
    const=65,
    default=65,
    help="%ID cutoff ",
)
args = parser.parse_args()


DATA_PATH = "/global/homes/v/vsevim/scratch/ML/blast/data/seqs/"
MIN_PID  = float(args.min_pid)
MIN_DIST = 1-(MIN_PID/100)

print("Running for %ID cutoff = ", MIN_PID)

'''
    Read sequences
'''
df_seqs  = pd.read_csv(DATA_PATH + "NR_DISTINCT_FAAs.tsv", sep="\t", header=None)
df_seqs  = df_seqs.rename(columns={0:"taxid", 1:"proteinid",2:"seq"})
print("#seqs in tsv= ", len(df_seqs))



'''
    Read pairs
'''
df_pids  = pd.read_csv(DATA_PATH + "SAMPLED_PROTEINS.faa.blast.tsv", 
                       header=None, 
                       sep="\t",
                       dtype={0:np.int64, 1:np.int64, 2:np.float16,\
                              3:np.int32, 4:np.int32, 5:np.int32, \
                              6:np.int32, 7:np.float64, 8:np.float32}
)

df_pids  = df_pids.rename(columns={0:'queryid',1:'targetid',2:'pidentity',\
                                   3:'length',4:'mismatch',5:'qlen',\
                                   6:'tlen',7:'evalue',8:'bitscore'}
           )

df_pids['distance'] = 1. - (df_pids.pidentity/100.)
df_pids = df_pids[df_pids.distance <= MIN_DIST]


'''
    Append seqs to pairs
'''


dummy_q = []
dummy_t = []
for i, row in df_pids.iterrows():
    qid = int(row.queryid)
    tid = int(row.targetid)
    #print(qid) 
    
    qseq = df_seqs[df_seqs.proteinid==qid].seq.values[0]
    tseq = df_seqs[df_seqs.proteinid==tid].seq.values[0]
    
    dummy_q.append(qseq)
    dummy_t.append(tseq)

    if(i%10000 == 0):
        print(i, end=",", flush=True)
        
df_pids['qseq'] = dummy_q
df_pids['tseq'] = dummy_t



'''
    Write into df.
'''
df_pids.to_pickle(DATA_PATH + "DF_SAMPLED_PROTEINS_PIDS.pkl.gz")
print("Written DF. Len =", len(df_pids))

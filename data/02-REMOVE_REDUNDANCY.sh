base=`basename $1 .tsv`
dirname=`dirname $1`
MINLEN=$2
MAXLEN=$3

tsv_out="$dirname/NR_""$base.tsv"
faa_out="$dirname/NR_""$base"".faa"

echo "Removing redundancy from $base, minlen=$2, maxlen=$3..."

# Also remove sequences with X in them.
#sort -u -k3,3 -t$'\t' "$1" | awk -F'\t' -v MAXLEN=$MAXLEN -v MINLEN=$MINLEN \
#    '{if((length($3)>=MINLEN) && (length($3)<=MAXLEN) && ($3 !~ /^[G,A,L,M,F,W,K,Q,E,S,P,V,I,C,Y,H,R,N,D,T]+$/)) print $1"\t"$2"\t"$3}' > "$tsv_out"

# Also remove sequences with X in them.
awk_line='
{
    if( (length($3)>=MINLEN) && 
        (length($3)<=MAXLEN) && 
        ($3 ~ /^[G,A,L,M,F,W,K,Q,E,S,P,V,I,C,Y,H,R,N,D,T]+$/)) 
        print $1"\t"$2"\t"$3;
}
'
sort -u -k3,3 -t$'\t' "$1" | awk -F'\t' -v MAXLEN=$MAXLEN -v MINLEN=$MINLEN "$awk_line" > "$tsv_out"


count=`wc -l "$tsv_out"`
echo "Redundancy removed. Written $count records into $tsv_out" 

awk '{print ">"$2"\n"$3}' $tsv_out > $faa_out  

# pass the non-redundant.faa as argument 
module load blast+

base=`basename $1 .faa`
echo $base
#makeblastdb -in "$1" -blastdb_version 5 -title "$base" -dbtype prot -out "blastdb/$base"
makeblastdb -in "$1" -title "$base" -dbtype prot -out "blastdb/$base"

#mv "$1".p?? ./blastdb
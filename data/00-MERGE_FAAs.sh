MY_PATH="/global/dna/projectdirs/microbial/img_web_data_secondary/taxon.faa"
SCRIPT_PATH="/global/u2/v/vsevim/s/fasta_util"

echo "Merge started"
infile="$1"
outfile="$2"

while read line; do 
    fname=`echo $line | awk '{printf "%s%s", $1, ".faa"}'` 
    if [ -e "$MY_PATH/$fname" ]; then
        cat "$MY_PATH/$fname"
    fi      
done<"$infile" > "$outfile"

echo "Merging done."

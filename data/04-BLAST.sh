module load blast+
NUM_THREADS=30

echo "Blasting $1"
DB_NAME=$2
echo "Blast db name: $DB_NAME"
EVALUE=1E-6

blastp  -db "$DB_NAME" \
		-query "$1" \
		-out "$1.blast.tsv" \
		-outfmt "6 qseqid sseqid pident length mismatch qlen slen evalue bitscore" \
		-evalue $EVALUE \
		-num_threads $NUM_THREADS

echo "Blast completed."
import numpy as np
import pandas as pd
import random
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder

AMINOACID_ALPHABET_SIZE = 20
N_SYMBOLS = AMINOACID_ALPHABET_SIZE + 1
n_symbols = N_SYMBOLS

aminoacid_alphabet = [
    'G', 'A', 'L', 'M', 'F', 'W', 'K', 'Q', 'E', 'S', 'P', 'V', 'I', 'C', 'Y',
    'H', 'R', 'N', 'D', 'T'
]

ALPHABET_W_PADDING = [
    '0', 'G', 'A', 'L', 'M', 'F', 'W', 'K', 'Q', 'E', 'S', 'P', 'V', 'I', 'C',
    'Y', 'H', 'R', 'N', 'D', 'T'
]

aa_dict = {
    '0': 0,
    'G': 1,
    'A': 2,
    'L': 3,
    'M': 4,
    'F': 5,
    'W': 6,
    'K': 7,
    'Q': 8,
    'E': 9,
    'S': 10,
    'P': 11,
    'V': 12,
    'I': 13,
    'C': 14,
    'Y': 15,
    'H': 16,
    'R': 17,
    'N': 18,
    'D': 19,
    'T': 20
}

seq_choices = ALPHABET_W_PADDING


def pad_seqs_to_onehot(batch,
                       padded_len,
                       max_input_len,
                       random_padding=False,
                       center_padding=False,
                       flat=False,
                       dtype=None):

    max_input_len = max([len(x) for x in batch])

    if flat:
        new_batch = np.zeros(
            (len(batch), padded_len * AMINOACID_ALPHABET_SIZE), dtype=dtype)
    else:
        new_batch = np.zeros((len(batch), padded_len, AMINOACID_ALPHABET_SIZE),
                             dtype=dtype)

    if random_padding:
        # Pad randomly from left and right to padded_len.
        assert padded_len >= max_input_len + 2, \
               "Padded len needs to be >=max_input_len+2."                              

        for i, seq in enumerate(batch):
            n_tot_pad = padded_len - len(seq)
            n_r_pad = random.randint(0, n_tot_pad)
            n_l_pad = n_tot_pad - n_r_pad
            r_pad = '0' * n_r_pad
            l_pad = '0' * n_l_pad
            new_seq = l_pad + seq + r_pad
            new_batch[i] = seq_to_vec(new_seq, flat=flat, dtype=dtype)
    else:
        ''' 
           Pad from the left or the center
        '''
        assert padded_len >= max_input_len, \
               "Padded len needs to be >=max_input_len."

        for i, seq in enumerate(batch):
            if center_padding:
                new_seq = seq.center(padded_len, "0")
            else:
                new_seq = seq.rjust(padded_len, "0")

            new_batch[i] = seq_to_vec(new_seq, flat=flat, dtype=dtype)

    return new_batch


# Fast seq_to_vec


def seq_to_vec(seq, flat=False, dtype=None):
    myseq = list(seq)
    int_encoded_seq = [aa_dict[x] for x in myseq]

    targets = np.array(int_encoded_seq).reshape(-1)
    one_hot_targets = np.eye(N_SYMBOLS)[targets].astype(dtype)

    return one_hot_targets[:, 1:]


def sklearn_seq_to_vec(seq, dtype=np.uint8, flat=False):
    myseq = list(seq)
    le = preprocessing.LabelEncoder()
    le.fit(ALPHABET_W_PADDING)
    int_alphabet = le.transform(ALPHABET_W_PADDING).reshape(
        len(ALPHABET_W_PADDING), 1)

    int_encoded_seq = le.transform(myseq)
    int_encoded_seq = int_encoded_seq.reshape(len(int_encoded_seq), 1)
    #print(int_encoded_seq)

    ohe = OneHotEncoder(categories='auto',
                        dtype=np.uint8,
                        sparse=False,
                        drop='first')
    ohe.fit(int_alphabet)

    return ohe.transform(int_encoded_seq)


def original_seq_to_vec(seq, flat=False, dtype=None):

    seq_len = len(seq)
    seq_ind = ["-"] * seq_len

    for i, aa in enumerate(seq):
        seq_ind[i] = seq_choices.index(aa) - 1
    vec = [0] * seq_len * (N_SYMBOLS - 1)

    for i, ind in enumerate(seq_ind):
        if ind >= 0:
            vec[i * (N_SYMBOLS - 1) + ind] = 1

    if dtype != None:
        vec = np.array(vec, dtype=dtype)
    else:
        vec = np.array(vec)

    if flat:
        return vec
    else:
        return np.reshape(vec, (seq_len, AMINOACID_ALPHABET_SIZE))


def vec_to_seq(vec):
    seq = ""
    for aa_vec in vec:
        if 1 not in aa_vec:
            seq += '0'
        else:
            seq += seq_choices[np.argmax(aa_vec) + 1]
    return seq


def seq_to_intcoded(seq, pad_len):
    myseq = list(seq)
    int_encoded_seq = [aa_dict[x] for x in myseq]

    L = len(myseq)
    assert pad_len >= L, print(L, pad_len)

    padded = [0] * (pad_len - L)
    padded.extend(int_encoded_seq)

    return padded


def intcode_all(batch, pad_len):
    new_batch = np.zeros((len(batch), pad_len))

    for i, seq in enumerate(batch):
        new_batch[i] = seq_to_intcoded(seq, pad_len)

    return new_batch


def date_stamp():
    from datetime import datetime
    now = datetime.now()
    date_time = now.strftime("%Y_%m_%d__%Hh%Mm")
    return date_time

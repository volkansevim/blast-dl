#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
import numpy as np
import pandas as pd
import pickle
import os
import matplotlib.pyplot as plt
import time
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Conv1D, ZeroPadding2D, Activation, Input, concatenate
from tensorflow.keras.models import Model
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import GlobalMaxPool1D, GlobalAvgPool1D, MaxPooling1D
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda, Flatten, Dense,ReLU
from tensorflow.keras.initializers import glorot_uniform
from tensorflow.keras.layers import Layer
from tensorflow.keras.regularizers import l2
from tensorflow.keras.utils import Sequence
from tensorflow.keras import activations
import tensorflow.keras.backend as K
from sklearn.utils import shuffle
import numpy.random as rng
from common_funcs_2 import pad_seqs_to_onehot
import common_funcs_2 as cf
import argparse


class DataGenerator(Sequence):
    def __init__(self, df, batch_size=32, shuffle=True):

        """
        Initialization
        """
        self.df = df
        """ 
            Each sample is repeated k times to create 
            variationally padded self-self pairs.. 
        """
        self.shuffle = shuffle
        self.epoch = 1
        self.batch_size = batch_size
        self.batch_count = 0
        self.batches_per_epoch = int(len(df)/batch_size)
        self.on_epoch_end()

    def __len__(self):
        #'Denotes the number of batches per epoch'
        return self.batches_per_epoch

    def __getitem__(self, index):
        selected_records = []
        self.batch_count += 1

        if self.batch_count >= self.batches_per_epoch:
            self.batch_count = 0
            self.epoch += 1

        df_batch = self.df.sample(self.batch_size)
        
        np_len = np.vectorize(len) 
        all_seqs = np.append(df_batch.qseq.values, df_batch.tseq.values)   
        max_seq_len = np.max(np_len(all_seqs))
        padded_len = max_seq_len + 10
        
        query_batch = pad_seqs_to_onehot(
            df_batch.qseq.values,
            padded_len,
            max_seq_len,
            random_padding=False,
            dtype=np.int8,
            center_padding=True,
        )

        target_batch = pad_seqs_to_onehot(
            df_batch.tseq.values,
            padded_len,
            max_seq_len,
            random_padding=False,
            dtype=np.int8,
            center_padding=True,
        )
                
        training_distances = df_batch.distance.values
        training_distances = training_distances.astype(np.float16)
        return [query_batch, target_batch], training_distances

    def on_epoch_end(self):
        pass

    def __data_generation(self, list_IDs_temp):
        pass



class ResidualUnit(Layer):
    def __init__(self, filters, strides=1, activation="relu", **kwargs):
        super().__init__(**kwargs)
        self.activation = activations.get(activation)
        self.main_layers = [
            Conv1D(filters, 3, strides=strides, padding="same", use_bias=False),
            BatchNormalization(),
            self.activation,
            Conv1D(filters, 3, strides=1, padding="same", use_bias=False),
            BatchNormalization(),
        ]

        self.skip_layers = []
        if strides > 1:
            self.skip_layers = [
                Conv1D(filters, 1, strides=strides, padding="same", use_bias=False),
                BatchNormalization(),
            ]

    def call(self, inputs):
        Z = inputs
        for layer in self.main_layers:
            Z = layer(Z)
        skip_Z = inputs
        for layer in self.skip_layers:
            skip_Z = layer(skip_Z)
        return self.activation(Z + skip_Z)


# In[14]:


def get_siamese_model(padded_len, output_dim=300):
    input_shape = (padded_len, 20)
    print(input_shape)
    
    # Define the tensors for the two input images
    left_input = Input(input_shape)
    right_input = Input(input_shape)
    
    # Convolutional Neural Network
    model = Sequential()
    model.add(
        Conv1D(
            64, 3, strides=1, input_shape=input_shape, padding="same", use_bias=False
        )
    )
    model.add(BatchNormalization())
    model.add(Activation("relu"))
    model.add(MaxPooling1D(pool_size=2, strides=1, padding="same"))
    prev_filters = 64
    for filters in [64] * 3 + [128] * 4 + [256] * 6 + [512] * 3:
        strides = 1 if filters == prev_filters else 2
        model.add(ResidualUnit(filters, strides=strides))
        prev_filters = filters
    model.add(GlobalAvgPool1D())
    model.add(Flatten())
    model.add(Dense(output_dim, activation=None))
    #model.add(Lambda(lambda x: tf.math.l2_normalize(x, axis=1))) 
    
    # Generate the encodings (feature vectors) for the two images
    encoded_l = model(left_input)
    encoded_r = model(right_input)
    
    # Add a customized layer to compute the absolute difference between the encodings
    L1_layer = Lambda(lambda tensors:tf.keras.backend.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([encoded_l, encoded_r])

    #Add euclidean distance layer
    #L2_layer = Lambda(lambda tensors:K.sqrt(K.sum(K.square(tensors[0] - tensors[1]), axis=1, keepdims=True)))
    #L2_distance = L2_layer([encoded_l, encoded_r])
    
    # Add a dense layer with a sigmoid unit to generate the similarity score
    prediction = Dense(1,activation='linear')(L1_distance)
    
    # Connect the inputs with the outputs
    siamese_net = Model(inputs=[left_input,right_input],outputs=prediction)
    
    # return the model
    return siamese_net




'''
    Main

'''

parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

parser.add_argument(
    "-p",
    "--checkpoint",
    nargs="?",
    const="",
    help="timestamp of checkpoint to load.",
)

parser.add_argument(
    "-e",
    "--epochs",
    nargs="?",
    const=10,
    default=10,
    type=int,
    help="Epochs",
)


args = parser.parse_args()

time_stamp = cf.date_stamp()
DATA_PATH = "/global/homes/v/vsevim/scratch/ML/blast/data/seqs/"
SAVE_PLOTS_DIR = "/global/homes/v/vsevim/scratch/ML/blast/save/plots/"
SAVE_WEIGHTS_DIR = "/global/homes/v/vsevim/scratch/ML/blast/save/weights/"
CHECKPOINT_DIR = "/global/homes/v/vsevim/scratch/ML/blast/save/chkpt/"

df_pids = pd.read_pickle(DATA_PATH + "DF_SAMPLED_PROTEINS_PIDS.pkl.gz")
print("Sequence set size:", len(df_pids))
#df_pids = df_pids.sample(100000, random_state=424242)


log_dir = "tboard/" + time_stamp
checkpoint_path = CHECKPOINT_DIR + time_stamp + ".ckpt"

tensorboard_callback = tf.keras.callbacks.TensorBoard(
    log_dir=log_dir, histogram_freq=1
)

cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path,
    save_weights_only=True,
    verbose=1
)


nplen = np.vectorize(len) 
all_seqs = np.append(df_pids.qseq.values, df_pids.tseq.values)   
max_seq_len = np.max(nplen(all_seqs))  
padded_len  = max_seq_len + 10


# In[16]:


model = get_siamese_model(padded_len)
optimizer = Adam(lr = 0.0005)
model.compile(loss="MSE", optimizer=optimizer)

if(args.checkpoint):
    checkpoint_path = CHECKPOINT_DIR + args.checkpoint + ".ckpt" 
    model.load_weights(checkpoint_path)
    print("Loadaed checkpint", args.checkpoint)
    print("Running for ", args.epochs, "epochs.")

epochs = args.epochs


# In[25]:


from sklearn.model_selection import train_test_split

df_train, df_test = train_test_split(df_pids, test_size=0.2, random_state=425364)
train_gen = DataGenerator(df_train, batch_size=128)
test_gen  = DataGenerator(df_test, batch_size=128)


'''
    Train
'''

history = model.fit(
    train_gen,
    epochs=epochs,
    verbose=1,
    use_multiprocessing=False,
    validation_data=test_gen,
    callbacks=[tensorboard_callback, cp_callback, ]
)

'''


'''

# In[ ]:

model.save_weights(SAVE_WEIGHTS_DIR + time_stamp + "-weights.hdf5")


df_sample = df_test.sample(n=10000)
q_seqs = df_sample.qseq.values.tolist()
t_seqs = df_sample.tseq.values.tolist()
blast_pid = df_sample.pidentity

q_seqs_ohe = pad_seqs_to_onehot(
    q_seqs,
    padded_len,
    max_seq_len,
    random_padding=False,
    center_padding=True
)

t_seqs_ohe = pad_seqs_to_onehot(
    t_seqs,
    padded_len,
    max_seq_len,
    random_padding=False,
    center_padding=True
)

# float16 greatly reduces df saving time.
start = time.time()    
predicted_pid = np.array(100 * (1 - model.predict([q_seqs_ohe, t_seqs_ohe]).flatten())) 


# In[ ]:


q_len = np.array([len(x) for x in q_seqs])
t_len = np.array([len(x) for x in t_seqs])
len_ratio = (q_len - t_len)/q_len
pid_ratio = blast_pid/predicted_pid



import scipy
R, p = scipy.stats.pearsonr(blast_pid, predicted_pid)
title = "Test Set: Pearson R={:.2f}\n{}".format(R, time_stamp)

fig = plt.figure(figsize=(6,6))
fig.patch.set_facecolor('white')
plt.axis((0., 100, 0., 100))
plt.scatter(blast_pid, predicted_pid, s=1)
plt.title(title)
plt.xlabel('Actual %ID')
plt.ylabel('Predicted %ID')
file_name = SAVE_PLOTS_DIR + time_stamp + "-actual_vs_predicted.png"
plt.savefig(file_name, format="png", pad_inches=0.3)



# In[ ]:


diff = np.array(blast_pid-predicted_pid)
mean = np.average(diff)
stdev = np.std(diff)
title = "Test Set: Mean={:.2f}, Stdev={:.1f}\n{}".format(mean,stdev, time_stamp)
fig = plt.figure(figsize=(6,6))
fig.patch.set_facecolor('white')
#plt.axis((0.5, 1, 0.5, 1))
plt.hist(diff, bins=25)
plt.title(title)
plt.xlabel('blast - predicted %ID')
plt.ylabel('Count')
file_name = SAVE_PLOTS_DIR + time_stamp + "-pid_deviation_dist.png"
plt.savefig(file_name, format="png", pad_inches=0.3)

print("R={:.2f}, mean={:.1f}, stdev={:.1f}".format(R, mean, stdev))
# In[ ]:


R, p = scipy.stats.pearsonr(pid_ratio, len_ratio)
title = "Test Set: Pearson R={:.2f}\n{}".format(R, time_stamp)
fig = plt.figure(figsize=(6,6))
fig.patch.set_facecolor('white')

#plt.axis((0.5, 1, 0.5, 1))
plt.scatter(pid_ratio, len_ratio, s=1)
plt.title(title)
plt.xlabel('blast %ID / predicted %ID')
plt.ylabel('q_len/t_len')
file_name = SAVE_PLOTS_DIR + time_stamp + "-length_diff_vs_deviation.png"
plt.savefig(file_name, format="png", pad_inches=0.3)



# In[ ]:




